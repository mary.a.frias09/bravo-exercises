In this lesson we will look into how to control where each element sits on a page and how to understand layouts within position property.
Why do we use positioning, and is there already a default position applied.

Discussing the Normal flow - Every block-level element appears on a new line, causing each item to appear lower down the page than the previous one. Even if you specify the width
of the boxes and there is space for two elements to sit side-by- side, they will not appear next to each other. This is the default behavior (unless you tell the browser to do something else).

NORMAL FLOW(static)-Every block-level element appears on a new line, causing each item to appear lower down the page than the previous one. Even if you specify the width
of the boxes and there is space for two elements to sit side-by- side, they will not appear next to each other. This is the default behavior (unless you tell the browser to do something else).

RELATIVE POSITIONING-This moves an element from the position it would be in normal flow, shifting it to the top, right, bottom, or left of where it would have been placed. This does not affect the position of surrounding elements; they stay in the position they would be in normal flow.

ABSOLUTE POSITIONING-This positions the element in relation to its containing element. It is taken out of normal flow, meaning that it does not affect the position
of any surrounding elements (as they simply ignore the space it would have taken up). Absolutely positioned elements move as users scroll up and down the page.

FIXED POSITIONING-This is a form of absolute positioning that positions the element in relation to the browser window, as opposed to the containing element. Elements with fixed positioning do not affect the position of surrounding elements and they do not move when the user scrolls up or down the page.

FLOATING ELEMENTS-Floating an element allows you to take that element out of normal flow and position
it to the far left or right of a containing box. The floated element becomes a block-level element around which other content can flow.
         
What do we change when we use a position property attribute that is not static?
 We are changing the normal document flow (static) and the positioning context.
 
 
We can change the normal document flow by adding the top, left, right and bottom property, but if you apply these you need a reference.

-Begin exploring ways to position elements using relative positioning, absolute positioning, fixed positioning and static positioning.


TODO: Add position: absolute to the header in css document.
(INSPECT THE PAGE DISCUSS)

TODO: Add top:0; and left:0; to the header in css.
Refresh the browser and discuss.

TODO: Remove the margin and discuss.

TODO:Add position: absolute to the body element in css.
Ex: body {background-color: orange; padding: 10px; margin: 0;
position: absolute;}
(Refresh the browser and discuss by adding position to the body containing element.)

TODO: Add top:0; to the body element
(discuss)

TODO: Make the body css back to original code
background-color: yellow;
    padding: 10px;
    margin: 0;
    
TODO: To the css body Add position relative, refresh the browser then go back add top:0; 
(Discuss nothing happens)

TODO: Add top:50px;
(Discuss we have kicked the element down by 50px;) 

TODO: Add position: relative to the nav in css.
(No effect)

TODO: Add top:100px; to the nav in css
(Discuss the nav moved 100px down but footer did not move up because the relative position does not take the elements out of the normal document flow)

TODO: Change 100px; to 10px in nav to put the page somewhat back to the beginning.

TODO: In the footer add position: fixed;

TODO: Add top:0; 
(Discuss we have added the element to the top of our page and this will be the fixed position depending on the viewport.)

TODO: Remove margin from nav property in css.
(Now we've added the element to a position referring to viewport )

TODO: Add height:3000px; to the html in css.
(Discuss scroll and see how the footer element sticks to the top of the page, because it is fixed.)




