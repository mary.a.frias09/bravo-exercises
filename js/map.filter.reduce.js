'use strict';

//MAP, FILTER, REDUCE
//- all functions that operates on collections (arrays)
//- all will NOT modify but will return a new copy of the array



// .map = transform each element in the collection
//.filter = filters our values
//.reduce = reduce a collection to a single value


// var numbers = [1,2,3,4,5,6,7,8,9,10];

// var evens = [];
//
// for (var i = 0; i < numbers.length; i++){
//     if (numbers[i] % 2 === 0) {    //
//         evens.push(numbers[i]);   //adding a new array
//     }
// }
// console.log(evens);
//


// var odds = [];
// for (var i = 0; i < numbers.length; i ++){
//      if (numbers[i] % 2 === 1){     //finds all the odd numbers (1)
//         odds.push(numbers[i]);
//     }
// }
// console.log(odds);




// const numbers = [1,2,3,4,5,6,7,8,9,10];


//reCreating in an es6 arrow function
// const evens = numbers.filter(n => n % 2 === 0);




// const evens = numbers.filter(function (n) {
//     return n % 2 === 0;
// });
// console.log(evens);




// var odds = numbers.filter(function (n) {
//     return n % 2 === 1;
// });
// console.log(odds);



//Creating in an es6 arrow function
// const odds = numbers.filter(n => n % 2 === 1);





//
// // MAP = callback functions
// var increment = numbers.map(function (num) {
//     return num + 1;
// });
//
// console.log(increment);
//
//
//
// // Creating in an es6 arrow function
// const increment = numbers.map(num => num + 1);




//REDUCE
//.reduce = used to 'reduce' a collection to a single value.

// const nums =  [1,2,3,4,5];
// const sum = nums.reduce((accumulation, currentNumber) => {
//     return accumulation + currentNumber;
// }, 100);
//
// console.log(sum);



// const officeSales = [
//     {name: 'Jim Halpert', sales: 500},
//
//     {name: 'Dwight Schrute', sales: 750},
//
//     {name: 'Ryan Howard', sales: 150},
// ];
//
// const totalSales = officeSales.reduce((total, person) => {
//     return total + person.sales;
// }, 0);
// console.log(totalSales);


//
//**********************************************************************************
//




