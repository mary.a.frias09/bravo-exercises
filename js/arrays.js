(function () {
    "use strict"
    console.log("Hi, from Me");
    //
    // /** TODO:
//  *  Create a new EMPTY array named sports. **
//
//     var sports = [];
//
//         console.log(sports);

//  *  Create a new EMPTY array named sports. **

    // sports.push('Basketball','Football','Baseball','Softball','Volleyball');
    //
    //      console.log(sports);

// /** Sort the array. **

    //     var sports = ['Basketball' , 'Football', 'Baseball', 'Softball', 'Volleyball'];
    //
    // sports.sort();
    //
    //     console.log(sports);


// /** Reverse the sort that you did on the array.**
//     sports.reverse();
//
//         console.log(sports);
//
//
// // /** Push 3 more sports into the array **
//     sports.push('Gymnastics', 'Golf', 'Hockey');
//
//         console.log(sports);


// /** Locate your favorite sport in the array and Log the index. **
//    var index = sports.indexOf('Football');
//
//         console.log(index)
//

        // console.log(sports[2]);



// /** Remove the 4th element in the array

    // var removedSport = sports.splice(4,1);

//     var removedSport = sports.shift('Gymnastics');
//     console.log('Here is the sport I removed:' + removedSport);




// /** Remove the first sport in the array
//
//     sports.shift();
//
//         console.log(sports);
//


// /** Turn your array into a string

    // sports.join(' , ')
    //
    //     console.log(sports);




//************************************************************************************************************





// /** Create a new array named bravos that holds the names of the people in your class**/
    //array starts at indexing of zero
    var bravos = ['maryann', 'sandra', 'henry', 'jonathan', 'adrian', 'eric', 'tameka'];

        console.log(bravos)





// /** Log out each individual name from the array by accessing the index. **/

        console.log(bravos.indexOf('maryann'));
            console.log(bravos.indexOf('sandra'));
                console.log(bravos.indexOf('henry'));






// /** Using a for loop, Log out every item in the bravos array **/
//
    for (let i = 0; i < bravos.length; i++){

        console.log(bravos[i])
    }





// /** Refactor your code using a forEach Function **/


    var bravos = ['maryann', 'sandra', 'henry', 'jonathan', 'adrian', 'eric', 'tameka'];

    bravos.forEach(function (name) {

        console.log(name)

    })






// /** Using a for loop. Remove all instance of 'CodeBound' in the array **/
// var arrays1 = ["fizz", "CodeBound", "buzz", "fizz", "CodeBound", "buzz"];


    var arrays1 = ["fizz", "CodeBound", "buzz", "fizz", "CodeBound", "buzz"];

        for(var n = 0; n < arrays1.length; n++){

        if(arrays1[n] !== 'CodeBound'){

            console.log(arrays1[n]);
        }

    }


// /**Using a forEach loop. Convert them to numbers then sum up all the numbers in the array **/
// var numbers = [3, '12', 55, 9, '0', 99];


    var numbers = [3, '12', 55, 9, '0', 99];
        var sum = 0;

        numbers.forEach(function (num) {

            sum += parseInt(num);

        });

        console.log(sum);




// /**Using a forEach loop Log each element in the array while changing the value to 5 times the original value **/

    var numbers = [7, 10, 22, 87, 30];

    numbers.forEach(function (element, index, numbers) {

        numbers[index] = element * 5;

    })
        console.log(numbers)







})()