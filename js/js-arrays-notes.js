(function () { //IIFE GOES FIRST
    "use strict" // STRICT GOES NEXT

//--------BASIC SYNTAX
// [] // an empty array
//     ['pink']//each individual value is considered an element
//     ['pink', 'purple', 'blue', 'black'] //4 elements
// //     [4, 6, 8, 10, 12]
//     [[2,3,4,5[2,4,6,8]]//nested arrays

    // var colors = ['black', 'green', 'purple', 'pink', 'blue', 'red']
    //
    // console.log(colors);
    // console.log(colors, length);

//--------Acessing arrays
//--------zero-indexed
//     var codebound = ['shephen', 'faith', 'karen', 'leslie', 'twyla'];
//     console.log(codebound[4])
//     console.log(codebound[3])
//     console.log(codebound[2])
//     console.log(codebound[1])
//     console.log(codebound[0])
//     console.log(codebound[5])//undefined
//
//     console.log('There are ' + codebound.length + ' people on the Codebound team! ')
//
//     console.log(codebound)

    //--------Iterating Array
    //--------Traverse through elements
    // var codebound = ['shephen', 'faith', 'karen', 'leslie', 'twyla']
    //
    // //loop through an array and console.log the values
    // for (var i = 0; i < codebound.length; i += 1);{
    //     console.log('The person at index ' + ' + i + 'is' + '  ' + codebound[i]);
    // }


    //------forEach Loop
    //------BASIC SYNTAX
        // array.forEach(function (element, index, array) {
        //
        // })
    //Keep naming conventions to plural nouns
    //var numbers = [2,3,4,5,6,7,8,9,10]
    // var colors = ['blue', 'black', 'purple', 'green']
    //
    //colors.forEach(function (color) {
    //     //do something
    //     console.log(color)
    //
    // })
    //

    //----------Changing/Manipulating Arrays
    // var fruits = ['banana', 'apple', 'grape', 'strawberry']
    //
    // //Adding to the array
    // //.push allows to add push method into array in the end
    // console.log(fruits)
    //
    // fruits.push('watermelon');
    // console.log(fruits)
    //
    // //.unshift add to the beginning of an array
    // fruits.unshift('cherry')
    // console.log(fruits);
    // fruits.push('mandarin', 'dragon fruit', 'starfruit');
    // console.log(fruits)
    //
    // //--------Remove/Delete from an array
    // //     .pop //remove the last element in the array
    // fruits.pop();
    // console.log(fruits);
    //
    // //     .shift //removes the first element in the array
    // fruits.shift();
    // console.log(fruits);
    //
    // var removedFruit = fruits.shift();
    // console.log('Here is the fruit I removed:' + removedFruit);

    //--------Locate Array Elements
    //   .indexOf //returns the first occurrence of what you are looking for
    // var fruits = ['banana', 'grape', 'apple', 'grape', 'strawberry']
    // //   .lastIndexOf
    // var index = fruits.indexOf('grape')
    // console.log(index)

    // // .lastIndexOf starts at the end of an array and returns index
    // index = fruits.lastIndexOf('grape')
    // console.log(index)

    //--------Slicing
    //.slice copies a portion of the array
    //will always return a new array-does not modify original array
    // var fruits = ['banana', 'grape', 'apple', 'strawberry', 'mandarin', 'dragon fruit', 'starfruit']
    //
    // var slice = fruits.slice(2,5);
    // console.log(slice);
    //
    // slice = fruits.slice(1);
    // console.log(slice);


    //--------REVERSE
    //.reverse
    // will reverse the original array AND return the reversed array
    // var fruits = ['banana', 'grape', 'apple', 'strawberry', 'mandarin', 'dragon fruit', 'starfruit']

    // fruits.reverse();
    // console.log(fruits);

    //--------SORTING
    // .sort
    //will sort the original array AND return the sorted array

    // var fruits = ['banana', 'grape', 'apple', 'strawberry', 'mandarin', 'dragon fruit', 'starfruit']
    //
    // fruits.sort();
    // console.log(fruits);
    //

    //--------SPLITTING
    //.split
    //takes a string and turns it into an array

    // var codebound = 'Karen,Faith,Stephen';
    // console.log(codebound);
    // var codeBoundArray = codebound.split(' , ')
    // console.log(codeBoundArray);

    //--------JOINGING
    //.join
    // //takes an array and converts it to a string with delimiter of your choice
    // var codeBoundArray = ['Karen,Faith,Stephen'];
    // console.log(codeBoundArray);
    // var newCodeBound = codeBoundArray.join(',');
    // console.log(newCodeBound);
    //



















})()

