"use strict";

//ES6 {ECMASCRIPT2015} is version of Javascript

//EXPONENT OPERATOR
// JS old way...
// Math.pow(2,8);
// console.log(Math.pow(2,8)); //256


//ES6
// console.log(2 ** 8); //256

//CONST keyword --do not use 'LET'
// - blocked scope variable(s)

//without a block scope
// if (true) {
//     var cohort = 'bravo';
// }
// console.log(cohort)//bravo

//with a block scope
// if(true) {
//     const cohort = 'bravo';
//
// }
// console.log(cohort);

// FOR LOOP with block scope

// for (var i = 0; i < 10; i++) {
//     console.log(i); // counts 0 - 9
// }


// FOR LOOP without block scope

// for (var i = 0; i < 10; i++) {

// }
// console.log(i);  //displays 10


//TEMPLATE STRINGS
// const  cohort = 'bravo';
// console.log('Hello' + cohort + '!'); //concatentation
//
// console.log('Hello ${cohort.toUpperCase()} !');

//FOR ... OF
//SYNTAX :
// for(const element of iterable) {

// }

// const x = [10, 20, 30];
// for (const value of x) {
//     console.log(value);
// }

//ARROW FUNCTIONS
// - shorthand function syntax, EXCEPT 'this' is bound lexically

//SYNTAX:
// js
// const sayHello = function (cohort) {
//     return 'Hello from ' + cohort;
// };

//arrow
// const sayHello = (cohort) => 'Hello from ' + cohort;

// DEFAULT FUNCTION PARAMETER VALUES
//old way
// function greeting(cohort) {
//     if (typeof cohort === 'undefined') {
//         cohort = 'World';
//     }
//     return console.log('Hello, ' + cohort);
// }
// greeting();
// greeting('bravo');

//
// arrow function
// const greeting = (bravo = 'World') => console.log('Hello from' ${bravo}');
//

//another new way
// function greeting() {

// }
// greeting('bravo');

//OBJECT PROPERTY VARIABLE ASSIGNMENT SHORTHAND
// objects from js
// var person = {
//     name: 'Bravo',
//     age: 2
// }
//
// const name = 'Bravo';
// const age = 2;
// const person = {
//     name,
//     age,
//     height: '5\'5\"'
// };

//OBJECT DESTRUCTURING
//shorthand for creating variable from object properties.

//old way..
// var person = {name: 'Bravo', age: 2};
// var name = person.name;
// var age = person.age;

// //new way...
// const person = {name: 'Bravo', age: 2};
// const {name, age} = person;

//old way...
// function personInfo(person) {
//     var name = person.name;
//     var age = person.age;
//
// }
//
//
//new way...
// function personInfo({name, age}) {
//     console.log(name);
// }


//DESTRUCTURING ARRAYS

// const myArray = [1, 2, 3, 4, 5];
// const [x, y, thirdIndex, bob, karen] = myArray;
// console.log(x);
// console.log(y);
// console.log(bob);









