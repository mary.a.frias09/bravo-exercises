"use strict"


// STANDARD JSON FORMAT
// {
//     "name1": "value",
//     "name2": "value2",
// }


//JSON can be any data type\
//
// {
//     "string": "stringvalue",
//     "num": 564,
//     "object": {
//         "property": "value"
//     },
//     "array": [
//         { "property": "value", "property2": "value2"}
//         ],
//     "boolean": true/false,
//     "null": null
// }
