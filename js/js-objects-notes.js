"use strict";




    //OBJECTS
    //Object is a grouping of data and functionality.
    //Data items inside of an object = PROPERTIES
    //Function inside of an object = METHODS



    //CUSTOM OBJECTS
    //Prototypes allows existing objects to be used as templates to create new objects.
    //Object keyword - starting point to make custom objects.

    // var car = new Object();
    // console.log(typeof car);
    //object

//The use of 'new Object()' calls the Object CONSTRUCTOR to build a new INSTANCE of object.






//OBJECT LITERAL NOTATION
//- curly braces {}

// var car = {};
// alert(typeof car);
// object
//




//SETTING UP PROPERTIES ON A CUSTOM OBJECT

// var car = {};
//
// // dot notation
// car.make = 'Toyota';
//
// //array notation
// car['model'] = '4Runner';
//
// console.log(car);




//Another way/ most common way to assign properties
//only '' applies to the property
// var car = {
//     make: 'Toyota',
//     model: '4Runner',
//     color: 'Black',
//     numberOfWheels: 4
// };
// console.log(car);
// console.log(model);




// //DON'T DO THIS
// car["numberOfDoors"] = 5;

//INSTEAD
// car.numberOfDoors = 5;
// console.log(car)




//NESTED VALUES
// var cars =[
//     {
//         make : 'Toyota',
//         model: '4Runner',
//         color: 'Black',
//         numberOfWheels: 4,
//         features:['Automatic Doors', 'Bluetooth Connection'],
//         alarm: function () {
// //             alert("Sound the alarm");
//         }
//     },
//     {
//         make: 'Honda',
//         model: 'Civic',
//         color: 'Green',
//         numberOfWheels: 4,
//         features: ['Great Gas Mileage', 'AM/FM Radio'],
//         owner: {
//             name:'John Doe',
//             age: 35
//         },
//         alarm: function () {
//             alert("No alarm....sorry");
//         }
//     }
// ];
// console.log(cars[0].features[1]);

// console.log(cars[1].owner.age);

//display the feature for both cars
// console.log(cars);
//
// cars.forEach(function (car) {
//
//     car.features.forEach(function (feature) {
//         console.log(feature)
//     });
// });
//

// console.log(cars[1].alarm());
//
// //remember functions
// function sayHello() {
//     console.log("Hello")
// }

// //call the function's name
// sayHello();





//The 'this' keyword
/*
'this' is simply a reference to the current object
- 'this' can refer to a different object based on how a function is called.
 */
//
// var car = {};
//
// car.make = 'Ford';
// car.model= 'Mustang';
//
// //creat a method on the car object
// car.describeCar = function () {
//     console.log("Car make and model is; " + this.make + ' ' + this.model);
// };
//
// //calling the object's method
// car.describeCar();

// var car = {
//     make: 'Ford',
//     model: 'Mustang',
//     desribeCar: function () {
//         console.log(`Car make and model is ${this.make} ${this.model}`);
//     }
// };
// car.desribeCar();
//
//



