"use strict";
console.log('hi');

const developers = [  //an array of properties
    {
        name: 'stephen',
        email: 'stephen@appddictionstudio.com',
        languages: ['html', 'css', 'javascript', 'php', 'java']
    },
    {
        name: 'karen',
        email: 'karen@appddictionstudio.com',
        languages: ['java', 'javascript', 'angular', 'spring boot']
    },
    {
        name: 'juan',
        email: 'juan@appddictionstudio.com',
        languages: ['java', 'aws', 'php']
    },
    {
        name: 'faith',
        email: 'faith@codebound.com',
        languages: ['javascript', 'java', 'sql']
    },
    {
        name: 'dwight',
        email: 'dwight@codebound.com',
        languages: ['html', 'angular', 'javascript', 'sql']
    },
    {
        name: 'maryann',
        email: 'maryann@codebound.com',
        languages: ['html', 'css', 'javascript']
    }
];



// TODO: replace the `var` keyword with `const`, then try to reassign a variable as a const
// TODO: fill in your name and email and add some programming languages you know to the languages array
// const name = 'maryann';
// const email = 'maryann@codebound.com';
// const languages = ['html', 'css', 'javascript'];


//TODO rewrite the object literal using object property shorthand
// developers.push({
//      name,
//      email,
//      languages
// });
// console.log(developers);




//TODO : replace `var` with `let` in the following variable declarations
// let emails = [];
// let names = [];


// TODO: rewrite the following using arrow functions
// developers.forEach(function(developer) {
//     return emails.push(developer.email);
// });
//
// arrow function
// developers.forEach(developer => emails.push(developer.email));
// console.log(emails);


// developers.forEach(function(developer) {
//     return names.push(developer.name);
// });

// arrow function
// developers.forEach(developer => names.push(developer.name));
// console.log(names);


// developers.forEach(function (developer) {
//     return languages.push(developer.languages);
// });

// arrow function
// developers.forEach(developer => languages.push(developer.languages));
// console.log(languages);



// TODO: replace `var` with `const` in the following declaration
// const developers = [];
// developers.forEach(function(developer) {




// TODO: rewrite the code below to use object destructuring assignment note that you can also use destructuring
//  assignment in the function parameter definition

//     const name = developer.name;
//     const email = developer.email;
//     const languages = developer.languages;
//
// const {name, email,languages} = developer;
//

//TODO: rewrite the assignment below to use template strings
//
//     developerTeam.push(name + '\'s email is ' + email + name + ' knows ' + languages.join(', '));
// });
//
//     developerTeam.push(`${name}'s email is ${email} ${name} knows ${languages}.join`)
// console.log(developers);






// TODO: Use `const` for the following variable
// const list = '<ul>';




// TODO: rewrite the following loop to use a for..of loop
// developers.forEach(function (developer) {


// TODO: rewrite the assignment below to use template strings
//     list += '<li>' + developer + '</li>';

// });

// list += '</ul>';

// document.write(list);

// FOR... OF LOOP
// for (const developer of developers ) {




// TODO: rewrite the assignment below to use template strings
//  list += '<li>' + developer + '</li>';

//     list += `<li>${developer}</li>`

// }
// list += '</ul>';

// for of loop SYNTAX:
// for (const element of iterable) {

// }




// *******************************************************************************************************************
//***************************MORE ES6 DRILLS*****************************************

// REWRITE THE FOLLOWING USING TEMPLATE STRINGS
// const petName = 'Chula';
// const age = 14;
//
// console.log("My dog is named " + name + ' and she is ' + age + ' years old.');
//
// console.log(`My dog is named ${petName} and she is ${age} years old`);






// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function addTen(num1) {
//     return num1 + 10;
// };

// const addTen = (num1) => num1 + 10;
// console.log(addTen(23));




// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function minusFive(num1) {
//     return num1 - 5;
// }

// const minusFive = (num1) => num1 - 5;
// console.log(minusFive(50));






// REWRITE THE FOLLOWING IN AN ARROW FUNCTION

// function multiplyByTwo(num1) {
//     return num1 * 2;
// }

// const multiplyByTwo = (num1) => num1 * 2;
// console.log(multiplyByTwo(500));








// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// const greetings = function (name) {
//     return "Hello, " + name + ' how are you?';
// };

const greetings = name =>
    console.log(`Hello ${name} how are you?` );

greetings('bob');




// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function haveWeMet(name) {
//     if (name === 'Bob') {
//         return name + "Nice to see you again";
//     } else {
//         return "Nice to meet you"
//     }
// }

const haveWeMet = name => (name === Bob) ? 'Nice to see you again':
    // : ('Nice to meet');

console.log(haveWeMet('alex'));
//TERNARY OPERATOR
























