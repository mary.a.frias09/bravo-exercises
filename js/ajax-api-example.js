"use strict";
var x = new XMLHttpRequest();
x.open('GET','https://swapi.dev/api/planets/1/');
x.onreadystatechange =
    function () {
        if (x.readyState === 4){
            document.getElementById('api-content').innerHTML = x.responseText;
        }
    };
function sendTheAPI() {
    x.send();
    document.getElementById('reveal').style.display = 'none';

}
