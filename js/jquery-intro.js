 "use strict";

// The jQuery Object
 //object used to find and creat HTML elements from the DOM

 // Document Ready
 // window.onload = function () {
 //     alert('The page has finished loading')
 // }

//
// $(document).ready(function () {
//     alert('The page has finished loading');
// });

//In jQuery, we use the dollar sign '$', to reference the jQuery object
 // $ is an alias of jQuery.

 //jQuery Selectors
 //ID selector              #id                 selects a single elements with the given id attribute
 //CLASS selector           .class              selects all elements with the given class name
 //ELEMENT selector         element ex h1       selects all elements with the given tag name
//MULTIPLE selector    selector1, selector2,..  selects the combined results of all the specified selectors
 //ALL selector              *                  selects all elements

//SYNTAX for jQuery selectors:
 // $('selector')

 //.html - returns the html contents of selected element(s)
 //Similar to 'innerHTML' property

 //.css - allows us to change css properties for a element(s)
 //Similar to the 'style' property

 //ID Selector
//SYNTAX for selecting an element by an id:
 // $('#id-name');


 //CLASS SELECTOR
 //SYNTAX for selecting an element with a class name:
 // $('.classname')


 // $('.urgent').css('background-color', 'red');
 // $('.urgent').css('text-decoration', 'underline');
 // $('.urgent').css({'background-color': 'yellow', 'text-decoration': 'line-through'}); //multiple properties



//ELEMENT SELECTOR
 //SYNTAX: $('element_name')

 // $('p').css('font-size', '50px');



 //MULTIPLE SELECTOR
 // SYNTAX : $('selector1, selector2, ...')
//
// $('.urgent, p').css('background-color', 'orange');
// $('.urgent, p').css('text-decoration', 'line-through');
//
// $('.urgent, p').css({'background-color': 'orange', 'text-decoration': 'line-through'}); //**preferred code


 //ALL SELECTOR
//SYNTAX: $(*)

// $('*').css('border', '4px dotted red');













