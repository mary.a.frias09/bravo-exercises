"use strict"
// Conditional Statements

//if statements
//will only once if it meets certain conditions. That condition will return true of false

//Basic Syntax
// if (condition) {
//     //execute this code if the condition is met
// }

//Example
// var numberOfLives = 0;
// if (numberOfLives === 0) {
//     console.log("Game Over")
//     alert("Game Over")
// }

// if/else statements
// will will only once if it meets certain conditions. That condition will return true or false
// else statement- will only run once when the condition is false
// we can nest if/else statements, one after another. should end with else.


//Basic Syntax
// if (condition){
//     //code here runs once the condition is met
// } else {
//     //run this code if the condition is a false statement
// }

// Example
// var b = 1;
// if(b === 10) {
//     console.log('b is 10');
// } else if (b > 10 ) {
//     console.log('b is is greater 10')
// } else  {
//     console.log('b is NOT 10')
// }
//

// Switch Statements
// less duplicated and increases readability in code !
//Basic Syntax
// switch (condition) {
//     case //code that get executed:
//     break;
//     case '':
//     default'';// equivalent to else statement
//     break;
// }

// Example
// var color = 'Pink';
// switch (color) {
//     case 'blue' :
//         console.log('You chose blue')
//         break;
//     case 'Red':
//         console.log('You chose red')
//         break;
//     case "Pink":
//         console.log('Pink is the best color in the World')
//     default:
//         console.log('You chose the wrong color')
// }



// Example switch statements
// var weatherCondition = 'sunny'
//
// switch (weatherCondition) {
//     case 'sunny' :
//         console.log('Today is a sunny day')
//         break;
//     case 'cloudy' :
//         console.log('Today is a cloudy day')
//     case 'raining' :
//         console.log('Today is a rainy day')
//         break;
//     default:
//         console.log('Weather unknown')
//}


//Ternary Operators
//short hand way of creating if/else statements
// only used when there are two choices to be made

// Basic Syntax
//     (if this condition is true) ? run this code : otherwise run this code instead
//Example
var numberOfLives = 6;

(numberOfLives===0) ? console.log('Game Over') : console.log('I\'m still alive');





