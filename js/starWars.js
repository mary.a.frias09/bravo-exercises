"use strict";

// var x = new XMLHttpRequest();
// x.open('GET','Surprise.html');
// x.onreadystatechange =
//     function () {
//         if (x.readyState === 4){
//             document.getElementById('ajax-content').innerHTML = x.responseText;
//         }
//     };
// function sendTheAJAX() {
//     x.send();
//     document.getElementById('reveal').style.display = none;
//
// }

// $.ajax('data/starWars.json', {
//     type: 'get',
// }).done(function (data) {
//
//     $.each(data, function (index, value) {
//         console.log(value);
//         var name = value.name;
//         var height = value.height;
//         var mass = value.mass;
//         var birth = value.birth_year;
//         var gender = value.gender;
//
//         var char = '<tr>' + '<td>' + name + '</td>'
//                           + '<td>' + height + '</td>'
//                           + '<td>' + mass + '</td>'
//                           + '<td>' + birth + '</td>'
//                           + '<td>' + gender + '</td>'
//                           + '</tr>';
//
//         $('#insertCharacters').append(char)
//
//
//     })
// })



// SOLUTION SLACKED OUT
$(document).ready(function(){

    $.ajax({
        url: 'data/starWars.json',
        type: 'get',
        dataType: 'JSON',
        success: function(response){
            var len = response.length;
            for(var i=0; i<len; i++){
                var characters = response[i].id;
                var username = response[i].username;
                var name = response[i].name;
                var email = response[i].email;

                var tr_str = "<tr>" +
                    "<td align='center'>" + (i+1) + "</td>" +
                    "<td align='center'>" + username + "</td>" +
                    "<td align='center'>" + name + "</td>" +
                    "<td align='center'>" + email + "</td>" +
                    "</tr>";

                $("#characters tbody").append(tr_str);
            }

        }
    });
});