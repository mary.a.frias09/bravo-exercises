"user strict"
console.log("Hello from MaryAnnnnnn");

//  * Write a function named isOdd(x).
//  * This function should return a boolean value.

 function isOdd(x) {
    return x % 2 !==0;
       //  5 % 2 !==0;
 }
console.log(isOdd(false))

//  * Write a function named isEven(x).
//  * This function should return a boolean value.

 function isEven(x) {
    return x % 2 ===0;
       //  5 % 2 ===0;
 }
console.log(isEven(4));

//  * Write a function named isSame(input).
//  * This will return the input as the same in value and datatype return true.

function isSame(input) {
        return input;
     // return input === input;
}
console.log(isSame('hello'));


//  * Write a function named isSeven(x).
//  * This function should return a boolean value.
function isSeven(num) {
    var matchNumber = 7;
    return num === matchNumber;
}
console.log(isSeven(5));



//  * WriTe a function named addTwo(x).
//  * This should return the output plus 2.
function addTwo(x) {
    return x + 2;
}
console.log(addTwo(4));



//  * Write a function named isMultipleOfTen(x).
//  * This function should return a boolean value.
function isMultipleOfTen(x) {
    return x % 10 === 0;

}
console.log(isMultipleOfTen(50 ));


//  * Write a function named isMultipleOftwo(x).
//  * This function should return a boolean value.
function isMultipleOfTwo(x) {
    return x % 2 === 0;
}
console.log(isMultipleOfTwo(88));


//  * Write a function named isMultipleOfTwoAndFour(x).
//  * This function should return a boolean value.
function isMultipleOfTwoAndFour(x) {
    return x % 4 === 0 &&  x % 4 === 0;
}
console.log(isMultipleOfTwoAndFour(88));


//  * Write a function named isTrue(). This function should take in any input and return true
//  * if the input provided is exactly equal to `true` in value and data type.
function isTrue(x) {
    return x === true;
}
console.log(isTrue(false));
console.log("If true return true");

//  * Write a function named isFalse(x). This function should take in a value and returns a true
//  * if and only if the provided input is equal to false.
function isFalse(x) {
    return x === false;
}
// console.log("Is this false if so return true");
console.log(isFalse(false));

//  * Write a function named isVowel(x).
//  * This function should return a boolean value.
function isVowel(x) {
    var result;
    result = x == "A" ||
        x == "E" ||
        x == "I" ||
        x == "O" ||
        x == "U" ||
        x == "a" ||
        x == "e" ||
        x == "i" ||
        x == "o" ||
        x == "u";
    return result;
}
console.log(isVowel("A"))


//  * Write a function named triple(x).
//  * This will return an input times 3.
function triple(x) {
    return x * x * x;
}
console.log(triple(4));


//  * Write a function named QuadNum(x).
//  * This will return a number times 4.
function QuadNum(x) {
    return x * x * x * x;
}
console.log(QuadNum(5));


//  * Write a function named modulus(a,b).
//  * This will return the remainder when a / b.
function modulus(a,b) {
    return (a % b )
}
console.log(modulus(8,3))


//  * Write a function named degreesToRadians(x).
//  * This function should convert from degrees to radians
function degreesToRadians(x) {

    var pi = Math.PI;

    return x * (pi/180);
}
console.log(degreesToRadians(45));



//  * Write a function named absoluteValue(x).
//  * This will return absolute value of a number.
function absoluteValue(x) {

    return Math.abs(x);

}
console.log(absoluteValue(-5))


//  * Write a function named reverseString(x).
//  * This will return a string in reverse order.

function reverseString(x) {
   return x.split('').reverse().join('');

    // var newString = "";
}
console.log(reverseString('MaryAnn Rocks'));

//  **/