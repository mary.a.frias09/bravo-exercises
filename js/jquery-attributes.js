'use strict';
$(document).ready(function () {



// ATTRIBUTE METHODS
/*
    .html()
    .css()
    .addClass() - adds the specified class(es) to each of the set of matched elements
    .removeClass() - removes a single class, multiple classes or all classes
    .toggleClass() - add or remove one or more classes.

     GETTERS/SETTERS

     METHOD CHAINING
 */

// $('#codebound').click(function () {
//     $(this).html('Codebound Rocks!');
// });

// $('#name').css('color', 'firebrick').css('background-color', 'yellow')

//     var highlightStyling = {
//         'color': 'red',
//         'background-color': 'blue',
//         'font-size': '30px'
//     };
// $('#name').css(highlightStyling);


// .addClass
    //SYNTAX .addClass( class name );

    // $('.important').click(function () {
    //     $(this).addClass('highlighted')
    // });

// .removeClass
    //SYNTAX .removeClass (class name);


    // $('.important').dblclick(function () {
    //     $(this).removeClass('highlighted')
    // });

//.toggleClass()
    //SYNTAX .toggleClass('')

    $('.important').click(function () {
        $(this).toggleClass('highlighted')
    });










})
