"use strict";
console.log("Hello Peep");

// While Loops
// Create a while loop that uses console.log() to create the output shown below
// 2
// 4
// 8
// 16
// 32
// 64
// 128
// 256
// 512
// 1024
// 2048
// 4096
// 8192
// 16384
// 32768
// 65536

// var num = 2;
//     while (num < 65600){
//         console.log(num);
//     num *= 2;
// }


// For Loops
// Create a function name showMultiplicationTable that accepts a number and console.logs the
// multination table for that number
// Example.
// showMultiplicationTable(8) should output
// 8 x 1 = 8
// 8 x 2 = 16
// 8 x 3 = 24
// 8 x 4 = 32
// 8 x 5 = 40
// 8 x 6 = 49
// 8 x 7 = 56
// 8 x 8 = 64
// 8 x 9 = 72
// 8 x 10 = 80




//
// function showMultiplicationTable(numberInput){
//        for var(i = 1; i <= 10; i++){
//
//            var answer = numberInput * 1;
//
//            console.log(numberInput + 'x' + i + ' = ' + answer)
// }
// }
// console.log(showMultiplicationTable(8))
//




// Create a for loop that uses console.log to create the output shown below.
// 100
// 95
// 90
// 85
// 80
// 75
// 70
// 65
// 60
// 55
// 50
// 45
// 40
// 35
// 30
// 25
// 20
// 15
// 10
// 5
//
// for (var i = 100; i >= 5; i-=5){
//
//         console.log(i)
//     }
//



// Break and Continue
// Prompt the user for an odd number between 1 and 50. **
// Use a loop and a break statement to continue prompting the user if they enter invalid input.**
// Use a loop and the continue statement to output all the odd numbers between 1 and 50, except for the
// number the user entered.
// Output should look like this:

// var userInput= parseInt(prompt('Enter a number from 1 and 50'));
//
// if (userInput < 0 || userInput > 50){
//     alert('Invalid Input! Must enter a number between 1 and 50');
//
// } else {
//
//     for (var i =1; i <= 50; i++) {
//
//         if (i === userInput) {
//
//             console.log('Oops! We are skipping number: ' + i);
//             continue;
//
//         }
//
//         if (i % 2 === 0) {
//             console.log('Even number: ' + i);
//
//         } else {
//
//             console.log('Odd number: ' + i);
//
//         }
//     }
// }



// Number to skip is: 27
// Here is an odd number: 1
// Here is an odd number: 3
// Here is an odd number: 5
// Here is an odd number: 7
// Here is an odd number: 9
// Here is an odd number: 11
// Here is an odd number: 13
// Here is an odd number: 15
// Here is an odd number: 17
// Here is an odd number: 19
// Here is an odd number: 21
// Here is an odd number: 23
// Here is an odd number: 25
// Yikes! Skipping number: 27
// Here is an odd number: 29
// Here is an odd number: 31
// Here is an odd number: 33
// Here is an odd number: 35
// Here is an odd number: 37
// Here is an odd number: 39
// Here is an odd number: 41
// Here is an odd number: 43
// Here is an odd number: 45
// Here is an odd number: 47
// Here is an odd number: 49










// Write a for loop that will iterate from 0 to 20.
// For each iteration, it will check if the current number is even or odd, and report that to the
// screen (e.g. "2 is even").


// for (var x = 0; x <= 20; x++){
//
//     if(x === 0){
//
//         alert('zero is zero')
//
//     }else if(x % 2 === 0){
//
//         alert('Here is an even number:' + x)
//
//     }else if(x % 2 !== 0){
//
//         alert('Here is an odd number ' + x)
//
//     }else{
//
//         alert('I do not know that number')
//     }
// }


















