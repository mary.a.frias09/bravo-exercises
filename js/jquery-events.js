"use strict";
console.log('hello');
//How to use jQuery to attach event listeners to html elements

//MOUSE EVENTS
/*
.click() - event handler to the "click"

.dblclick() - event handler to the "double click"

.hover() - executed when the mouse pointer enters and leaves the element(s)
 */

//  MOUSE EVENTS IN JAVASCRIPT
// var eventElement = document.getElementById('#my-id');
//
// eventElement.addEventListener('click', function (e) {
//     alert('My id was clicked!');
// });

// MOUSE EVENTS IN jQuery
// .click
// SYNTAX: $( 'selector' ).click( handler );

// $('#bravo').click(function () {
//     alert('H1 with the id of bravo was clicked!');
// });

//.dblclick()
// $('#bravo').dblclick(function () {
//     alert('H1 with the id of bravo was clicked!');
// });


//.hover()
//SYNTAX: $(selector).hover( handlerIn, handlerOut )
// $('#bravo').hover(
//     // handlerIn anonymous function
//     function () {
//         $(this).css('background-color', 'blue');
//     },
//     //handlerOut anonymous function
//     function () {
//         $(this).css('background-color', 'white');
//     },
// );

// $('.important').hover(
//     //handlerIn anonymous function
//     function () {
//         $(this).css('background-color', 'red');
//     },
//     // handlerOut anonymous function
//     function () {
//         $(this).css('background-color', 'green');
//     },
// );


// KEYBOARD EVENTS IN jQuery
/*
Different types of keyboard events
    .keydown()
    .keypress()
    .keyup()
    .on()
    .off()
 */

// .keydown() - event handler to the "keydown" JavaScript event/trigger that event
// on an element
//SYNTAX: $(selector).keydown( handler )
// $(#my-form).keydown(function () {
//     alert('Hey! You pushed down a key in the form');
// });


//.keypress() - same as keydown, with one exception: Modifier keys (shift, control, esc..)
//will trigger .keydown() BUT NOT .keypress()


//.keyup() -
//SYNTAX: $(selector).keyup( handler )
// $(#my-form).keyup(function () {
//      alert('Hey! You pushed down a key in the form and it was released!');
//   });


// .on() and off()
//SYNTAX: $(selector).on( handler )

// $('#bravo').on('keydown', function () {
//     console.log('clicked!')
//     alert('on event triggered');
// });
//
// $('#bravo').off('click', function () {
//     alert('off even trigger')
// });

















