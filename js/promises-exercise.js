'use strict'
console.log('hiiiii')

// CREATE A FILE NAMED 'promises-exercise.js' INSIDE OF YOUR JS DIRECTORY
// Write a function name 'wait' that accepts a number as a parameter, and returns
// a PROMISE that RESOLVES AFTER THE PASSED NUMBER OF MILLISECONDS

// EX. wait(4000).then(() => console.log('You will see this after 4 seconds.'));
// EX. wait(8000).then(() => console.log('You will see this after 8 seconds.'));


// const wait = (delay) => {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             resolve(`You will see this after ${delay / 1000 } seconds`);
//         }, delay);
//     });
// };
// wait(4000).then((message) => console.log(message));


// const wait = new Promise((resolve, reject) => {
//     setTimeout(4000);
//     if(Math.random()>0.5){
//         resolve();
//     } else{
//         reject();
//     }
// });
// wait.then(() => console.log('You will see this after 4 seconds.'));


//*********Exercise :
//Write a function testNum that takes a number as an argument and returns a
// Promise that tests if the value is less than or greater than the value 10.

// function testNum() {
//     return new Promise ((resolve, reject) => {
//         if(Math.random()<10){
//             resolve('')
//         } else {
//
//         }
//     });
// )}
// ----------------solution
// const compareToTen = (num) => {
//     const p = new Promise(((resolve, reject) => {
//         if (num > 10){
//             resolve( num + 'is greater than 10 success')
//         } else {
//             reject( num + 'is less that 10 error!')
//         }
//     })
//         return p;
// }
// compareToTen(0)
//     .then(result => console.log(result))
//
//
//
// //--------------another solution from Adrian
// const toTen = (num) => new Promise ((resolve, reject) => {
//     if (num > 10){
//         resolve('greater than ten')
//     } else {
//         reject('less than ten')
//     }
//     })
// toTen (11)
//     .then(function (value) {
//         console.log(value)
//     })
//     .catch(function (error) {
//         console.log(error)
//     })


//Exercise 3:
//Write two functions that use Promises that you can chain! The first
// function, makeAllCaps(), will take in an array of words and capitalize
// them, and then the second function, sortWords(), will sort the words
// in alphabetical order. If the array contains anything but strings, it
// should throw an error.

//list of arrays with a string datatype
//create 2 list of arrays for the resolve and reject
const arrayOfWords = [ 'cucumber', 'tomatoes', 'avocado']
const complicatedArray = [ 'cucumber', 42, true]

const makeAllCaps = (array) => {
    //
    return new Promise((resolve, reject) => {

        var capsArray = array.map(word => {

            if (typeof word === 'string' ){

                return word.toUpperCase()
            } else {
                reject('Error:Not all items in the array are strings')   //message that coordinates with the function
            }
        })
        resolve(capsArray);
    })
}
//another object of sortWords
const  sortWords = (array) => {
    //creating a new promise
    return new Promise((resolve, reject) => {
        if (array){
            array.forEach((el)=> {
                if (typeof el !== 'string'){
                    reject('Error:Not all items in the array are strings')
                }
            })
            resolve(array.sort())
        }else {
            reject('Something went wrong with sorting the words in the array')
        }
    })
}
makeAllCaps(arrayOfWords)
    .then(sortWords)
    .then((result) => console.log(result))
    .catch(error => console.log(error))

makeAllCaps(complicatedArray)
    .then(sortWords)
    .then((result) => console.log(result))
    .catch(error => console.log(error))








