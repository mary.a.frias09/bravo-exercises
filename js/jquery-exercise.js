"use strict"

// <!-- - Add attributes to your elements, you will need both id and class attributes-->
// $('#name').css('font-size', '50px');

// $('#myHobbies').css('background-color', 'pink');

// $('.hometown').css({'background-color': 'pink', 'text-decoration': 'underline'});


// <!-- - Update the jQuery code to select and alert a different id-->
// alert($('#name').html());

// alert($('.hometown').html());

// <!-- - Use the same id on 2 elements. How does this change the jQuery selection?-->
// The order in which an id is called with be the first id with the second id duplicate
// alert($('#myHobbies').html());

// <!-- - Remove the duplicate id. Each id should be unique on that page.-->

// <!-- - Update your code so that at least 3 different elements have the same class name ‘codebound’-->

// <!-- - Using jQuery, create a border around all elements with the class ‘codebound’ that is 2 pixels around and red-->
// $('.codebound').css('border', '2px solid red');


// <!-- - Remove the class from one of the elements. Refresh and test the border has been removed-->
//YES
// <!-- - Give another element an id of ‘codebound’. Did this element get a border?-->
// NO


// <!--Element Selectors-->
// <!-- - Remove/comment out your jQuery code from the previous lesson-->
//DONE


// <!-- - Using jQuery, set the font-size of all ‘li’ elements to 24px-->
// $('li').css('font-size', '24px');

// <!-- - Change the font colors of all h1, p, and li element (all should be different colors)-->
// $('h1').css('color', 'pink');
// $('p').css('color','orange');
// $('li').css('color', 'purple');


// <!-- - Create a jQuery statement that alerts the contents of your h1 element(s)-->
// alert($('h1').html());

// var content = $('h1').html();
// alert(content);

// <!--Multiple Selectors-->
// <!-- - Change to font colors and sizes of all h1, p, and li elements to be the same.-->
// $('h1','p','li').css('color', 'blue');








//*********************************************************************************************************
//-------------------------------------------jQuery exercise----------------------------------------------
// <!--Add jQuery code that will change the background color of a ‘h1’ element when clicked.-->

    $('h1').click(function () {
        $(this).css('background-color', 'pink');
    });




// <!--Make all paragraphs have a font size of 18px when they are double clicked.-->

$('p').dblclick(function () {
        $(this).css('font-size', '28px');
    });




// <!--Set all ‘li’ text color to green when the mouse is hovering, reset to black when it is not.-->

    $('li').hover(
        function () {
            $(this).css('color', 'green');
        },
        function () {
            $(this).css('color', 'black');
        }
    );


// <!--Remove your custom jQuery code from previous exercises.-->
//DONE


// <!--Add an alert that reads "Keydown event triggered!" every time a user pushes a key down-->
//
//     $('*').keydown(function () {
//         alert('Keydown event triggered!')
//     });


// <!--Console log "Keyup event triggered!" whenever a user releases a key.-->

    // $(document).keyup(function () {
    //     console.log('Keyup event triggered!')
    // });
    //

// <!--BONUS:-->
// <!--Create a form with one input with a type set to text.-->
//
//
// <!--Using jQuery, create a counter for every time a key is pressed.-->

    $('#forms').keypress(function () {
        console.log(this.value.length);
});



