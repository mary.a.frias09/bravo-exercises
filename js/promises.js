'use strict';
console.log('hi')

//----------PROMISE METHOD-----------
//represents the completion (or failure) of an asynchronous operation and it's resulting value
//********A promise is a object in javascript placeholder for something that'll happen later--or container for future value
//PENDING -  initial state, neither resolved or rejected
//RESOLVED - operation completed successfully
//REJECTED - operation failed

//METHODS-----
//.fetch
//.then
//.catch


// fetch('http://localhost')
//     .then(response => console.log(response))
//     .catch(error => console.log(error));


// const myPromise = fetch('http://');
// myPromise.then(response => console.log(response));
// myPromise.catch(error => console.log(error));

// Promise.all
// accepts an array of promises, and resolves when all the individual promises have resolves

// Promise.race:
// accepts an array of promises, and resolves when the first promise resolves

//Chaining Promises together -
// the return value from a promises callback can itself be treated as a promise,
// which allows us to chain promises together. Lets look at an example
// Promise.resolve -
// to immediately return a resolved promise


//**We can create a promise object like so:

// const myPromise = new Promise((resolve, reject) => {
//     if(Math.random()>0.5){
//         resolve();
//     } else{
//         reject();
//     }
// });
// myPromise.then(() => console.log('resolved'));
// myPromise.catch(() =>  console.log('rejected'));



// **Discovering pending
// const myPromise = new Promise ((resolve, reject) => {
//     setTimeout( () => {
//         if(Math.random()>0.5){
//             resolve();
//         } else {
//             reject();
//         }
//     },1500);
// )};
// console.log(myPromise); //A pending promise
//
// myPromise.then(() => console.log('resolved');
//
//
// myPromise.then(() => console.log('resolved');
// myPromise.then(() => console.log('rejected'));



//Write a promise that simulates an api request:

//  function makeRequest() {
//     return new Promise( (resolve, reject)=>{
//         setTimeout(()=> {
//             if(Math.random()>0.1){
//                 resolve('Here is your new data');
//             } else {
//                 reject('Network connection error');
//             }
//             },1500);
//      });
//  }
//  const request = makeRequest();
//  console.log(request);
// request.then(message => console.log('Promises resolved'));
//  request.then(=> console.log('Promise resolved', message));
//
// request.catch(message => console.log('Promises resolved'));





// created a javascript data, and assigned a property key first then value second
const data = {username:'example'};
//instead of using request method you can use fetch
fetch('data/starwars.json', {   //enter surprise.html in input to get error fetch gone bad*****

    method: 'POST', //
    header: {
        'Content-Type': 'application/json' ,
    },
    body: JSON.stringify(data),
})
    .then(response => response.json())
    .then(data=> {   //get response from json
        console.log()
    })
    .catch((error) => {
        console.log('Error fetch gone bad, error');
    });





//*****beginning steps to create a new promise
//create an object
// const isMomHappy = true;
//
// //Promise      Phone is part of the promise
// const willGetNewPhone = new Promise(((resolve, reject ) =>{if(isMomHappy) {
//     const phone = {
//         brand: 'Samsung',
//         color: 'black'
//     };
//     resolve(phone);
// }else {
//     const reason = new Error('mom is not happy')
//     reject(reason);
// }}))
// //2nd promise
// const showOff = function (phone) {
//     const message = " hey friend I have a new " + phone.brand + ' ' + phone.color + ' phone ';
//     return Promise.resolve(message);
// }
// //call our promise
// const askMom = function () {
//     willGetNewPhone
//         .then(showOff)
//         .then(fulfilled => console.log(fulfilled)
//     )
//         .catch(error => console.log(error.message))}
//
// askMom();















