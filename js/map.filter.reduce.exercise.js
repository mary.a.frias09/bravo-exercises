"use strict";
// MAP FILTER REDUCE EXERCISE
const developers = [
    {
        name: 'stephen',
        email: 'stephen@appddictionstudio.com',
        languages: ['html', 'css', 'javascript', 'angular', 'php'],
        yearExperience: 4
    },
    {
        name: 'karen',
        email: 'karen@appddictionstudio.com',
        languages: ['java', 'javascript', 'spring boot'],
        yearExperience: 3
    },
    {
        name: 'juan',
        email: 'juan@appddictionstudio.com',
        languages: ['html', 'css', 'java', 'aws', 'php'],
        yearExperience: 2
    },
    {
        name: 'faith',
        email: 'faith@codebound.com',
        languages: ['node', 'npm', 'sql', 'javascript', 'java' ],
        yearExperience: 5
    },
    {
        name: 'dwight',
        email: 'dwight@codebound.com',
        languages: ['html', 'angular', 'javascript', 'sql'],
        yearExperience: 8
    }
];

/**Use .filter to create an array of developer objects where they have
 at least 5 languages in the languages array*/
//arrow function
// const developersMoreThanFiveLanguages = developers.filter(developer => developer.languages.length >= 5)
// console.log(developersMoreThanFiveLanguages);


//javascript function
// const developerlanguages = developers.filter(function (d) {
//     if(d.languages.length >=5){
//         return d
//     }
// });
// console.log(developerlanguages);

/**Use .map to create an array of strings where each element is a developer's
 email address*/

//arrow function
// const developerEmails = developers.map(developer => developer.email);

// console.log(developerEmails);



/**Use reduce to get the total years of experience from the list of developers.
 - Once you get the total of years you can use the result to calculate the average.*/
// const totalYearsExperience = developers.reduce((totalExp, developer)=>
// {
//     return totalExp + developer.yearExperience;
// },0);
// console.log(totalYearsExperience);
//
// const average = totalYearsExperience / developers.length;
//
// console.log(average);


/**Use reduce to get the longest email from the list.*/
//using ternary operator in return output
// const longestEmail = developers.reduce((a,b) => {
//     return a.email.length > b.email.length ? a : b;
// }).email;
//
// console.log(longestEmail);



/**Use reduce to get the list of developer's names in a single string
 - output:
 CodeBound Staff: stephen, karen, juan, faith, dwight*/

// const codeBoundStaff = developers.reduce((names,staff) => {
//     return names + staff.name + ' , ';
//     // return `${names} ${staff.name}, `;    //using template strings
// }, 'CodeBound Staff: ');
//
// console.log(codeBoundStaff);




/** BONUS: use reduce to get the unique list of languages from the list
 of developers
 */

