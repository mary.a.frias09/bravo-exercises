"use strict" //help us to create/write cleaner code

console.log("Hello from Console"); //method used to test our work in the console
console.log("Hi, from MaryAnn");

// This is a comment
/*
This is comment for multilines
 */
//
//DATA TYPES
// PRIMITIVE TYPES
// numbers, strings, boolean, undefined, null...objects

// VARIABLES
// var, const

// NUMBERS - 100, -10, 0, 0.23
// STRINGS - "Stephen", "email@example.com", "qweruio"
// BOOLEAN - true, false
// if a user is TwentyOne or older? true or false
// UNDEFINED - unassigned variables have this value and type

//var person = undefined;

//NULL - a special keyword for nothing



//*******OPERATORS******
//built-in operators
Math.cos(9);
Math.pow(4, 3)

//Basic arithmetic operators
// follow same rule of order


// 99.9 number
// 'false' string
// false boolean
// '0' string
// 0 number
//



//******* LOGICAL OPERATORS******
// RETURNS A BOOLEAN VALUE WHEN USED WITH BOOLEAN VALUES
// also used with non-boolean values but will return a non-boolean value
/*
 && (AND)
 || (OR)
 !  (NOT)
*/
/*
&& (AND)
-A user can edit a message if they are the admin && the author;
INPUT       OUTPUT
T && T      TRUE
T && F      FALSE
F && F      TRUE
F && T      FALSE

|| (OR)
-A user can edit a message if they are a registered user || the admin;
INPUT       OUTPUT
T || T      TRUE
T || F      TRUE
F || F      FALSE
F || T      TRUE

 !  (NOT)(The opposite)
-Redirect users if they are ! or logged in;
INPUT       OUTPUT
!T          FALSE
!F          TRUE
!!!!!!!T    FALSE

EXAMPLE

false && false || true : true
true  || false || true : true
true && false && false || true : true



//       ******* COMPARISON OPERATORS******

//WILL RETURN A LOGICAL VALUE BASED ON WHETHER THE COMPARISON IS TRUE
//can be any data type
//4 comparison operators

== : check if the value is the same
=== : check if the value and the data type are the same
!=  : check if the value is NOT the same
!==  :check if the value and the dada type are NOT the same
 */


//EXAMPLES
/*
4 == 4    : TRUE
4 === '4' : FALSE
4 != 5    : TRUE
4 == '4'  : TRUE
4 == 5    : FALSE
4 !== 4   : FALSE
4 !== '4' : TRUE

7 !== 5   : TRUE
7 != '5'  : TRUE
4 != '2'  : TRUE
4 !== '2' : TRUE
4 !==FALSE: TRUE
4 !== 4   : FALSE

<, >, <=, >=
const age = 21;

(21) < 18 : FALSE

age > 18  : TRUE
age > 21  : FALSE
age >= 21 : TRUE

age >= 21 || age < 18 : TRUE
    T     ||    F

age <= 21 || age > 18 : TRUE
    T     ||    T

age < 21  && age > 18 : FALSE
    F     &&     T


// SHORTHAND ASSIGNMENT OPERATORS
    y + x = x;
  Operator   Original    Shorthand
    +=      x + 1 = x;     x += 1;
    -=      x - 1 = x;     x -= 1;
    *=      x * 1 = x;     x *= 1;
    /=      x / 1 = x;     x /= 1;
    %=      x % 1 = x;     x %= 1;


//UNARY OPERATORS
+    (plus)
-   (negative)

++  (increment)  pre, post
--  (decrement)  pre, post

Increment
var x = 2;
++x; //   output:3    pre-increment
x++; //   output:2    post-increment

var = 4;
--x;//    output:3       pre-decrement
x--//     output:4       post-decrement




 */


//CONCATENATION
const name = 'Stephen';
const age = 13;

console.log(name);
console.log(age);

console.log('Hello, my name is ' + name + ' and I am ' + age);

// TEMPLATE STRING
console.log(`Hello, my name is ${name} and I am ${age}`);

// GET THE LENGTH OF A STRING
console.log(name.length);

const s = 'Hello World';
console.log(s.length);

// GET THE STRING IN UPPERCASE
console.log(s.toUpperCase());


//GET THE STRING IN LOWERCASE
console.log(s.toLowerCase());


// INDEX
console.log(s.substring(0, 5));   //HELLO
console.log(s.substring(6, 11));  //WORLD




//USER INTERACTION
// alert()

// alert('Hello there!');
// alert('Welcome to my website!');




// confirm
//var confirmed = confirm('Are you sure you want to continue?');
//console.log(confirmed);



// prompt
//var userInput = prompt('Enter your name: ');
//console.log('The user enter: ' + userInput);



// FUNCTIONS
// Function - is a reusable block of code that performs
// a specific task.



// SYNTAX FOR A FUNCTION
/*
function nameOfTheFunction(parameter1, parameter2) {
    code you want the function to do
}


// CALL A FUNCTION
// call it by its name with ()
nameOfTheFunction(); call the function

var result = nameOfTheFunction();
console.log(result);

nameOfTheFunction; this will NOT run the function


 */
function increment(num1) {
    return num1 + 1;
}

//call the function
console.log(increment(4))
//NaN - Not a Number

// function increment (4) {
// return 4 + 1; //  5
// }
//
// var five= increment(4);
// console.log(five); // 5

// ANONYMOUS FUNCTIONS
// Including a name for our function is actually optional.
// To define a function we can also create an anonymous function.
// Function without a name, and store it in a variable.

//SYNTAX FOR ANONYMOUS FUNCTION
var increment = function (num1) {
    return num1 + 1;
};

// CALLING AN ANONYMOUS FUNCTION
var two = increment(1);
console.log(two); //2

// ARGUMENTS/PARAMETERS
// argument = the value that a function is called with
// parameter = part of the function's definition.

function sum(a, b) {
    return a + b;
}
console.log(sum(10, 51));
// name of the function? sum
// how many parameters do we have? 2
// what are the names of the parameters? a and b
// what is function doing/ returning? adding a and b
// what will be the result for our console.log? 61

function sumPart2(a, b) {
    var result = a + b;
    return result;
}
console.log(sumPart2(10, 51));


// A FUNCTION WITH A RETURN VALUE
function shout(message) {
//    alert(message.toUpperCase() + '!!!');
}
var returnValue = shout('hey yall');
// console.log(returnValue);

// FUNCTION WITH NO PARAMETERS AND NO RETURN VALUE
function greeting() {
    alert('hey there!')
}
console.log(greeting());

// function greeting(message) {
//     return alert(message);
// }
//
// console.log(greeting('Hey CodeBound!'));

// function soSomething(input) {
//     var output;
//     return output;
//
// }
//
// function doSomething(input) {
//    var output = input;
//    return output;
//
// }


// Function Scope
//Block(local) vs. Global Variables


// Block(local) Variables
function ex() {
    var example = "Hello, My name is MaryAnn" //Local Variable
}

//Global Variables
var example = "Hello, My name is MaryAnn" //Global Variable
function ex() {
    //do something here
}










