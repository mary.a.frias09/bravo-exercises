(function () {
    "use strict"
    console.log("Hello");


    //--------------BOM LECTURE NOTES
    // Browser Object Model
    //hierarchy of objects in the browser.
    //we can target and manipulate HTML elements with JS.

    //objects that consists of the bom:
    //--Location/
        //location object/
        //manipulate the location of a document
        //get query string parameters

    //Navigator
        //navigator object
        //query the info and capabilities of the browser

    //Screen
        //screen object
        //manipulate the size, clone, get information about the screen

    //History
        //history
        //gets info/manage the web browser screen.

    //Window
    //Document - DOM

    //------------WINDOW OBJECT
    //the core of the BOM
    //window object represents the JS global object
    //all variables and functions declared globally with the var keyword become the properties of the window object
    //methods  in the window object--
    // alert()
    // confirm()
    // prompt()

    // setTimeout()//allows us to set a timer and execute a callback function once the timer expires.
    // setInterval()//will execute a callback function repeatedly with a fixed delay between each call.


    //----setTimeOut()

    //----BASIC SYNTAX
    //var timeoutID = setInterval(CB [,DELAY], arg1, arg2....);
    //delay is in milliseconds
    //delay will default to 0


//     function timeoutEx() {
//         setTimeout(function () {
//             alert("Hello Brave Cohort and welcome to CodeBound")
//
//         },7000);
//     }
//     timeoutEx();
//
    //----------setInterval()

    //BASIC SYNTAX
    // var intervalID = setInterval(CB, delay, [arg1, arg2,...])
    //CB is executed every delay millisecond
    //delay is the time that the timer should delay between executions of the CB function

    //clearInternal()//Allows us to stop.
    // clearInterval(intervalID);

    // function intervalEx() {
    //     setInterval(function () {
    //         alert("Hi Faith!")
    //     }, 3000);
    // }
    // intervalEx();
    // clearInterval();
    //
    //

    // var intervalID = setInterval(function () {
    //         alert("Hi Faith!")
    // },3000)
    // clearInterval(intervalID);
    //
    //
    //
    // var count = 0;
    // var max = 10;
    // var interval = 2000;
    //
    // var intervalID = setInterval(function () {
    //     if (count >= max){
    //         clearInterval(intervalID);
    //         console.log("Finished")
    //     } else{
    //         count++;
    //         console.log(count);
    //     }
    // },interval);


    //----------Document - DOM - Document Object Model (child of the BOM)
    //manipulate HTML using JS

    // Locating Elements
    //target by :
        //Element
        //Class
        //Id


        //getElementBy//
        //Basic Syntax
        //document.getElementsByName('Name of the Element/Class/Id');

    //Example
    // var btnClicked = document.getElementById('btn3');
    // console.log(btnClicked);


    // -------------Accessing form Inputs
    //we access forms using the form collections.

    // var usernameInput = document.forms.login.username;
    // console.log(usernameInput);


    //----------Accessing HTML Elements Using Class//
    // var cards = document.getElementsByClassName('card')
    // console.log(cards);
    // // cards.shift();//wont work because it comes back to us as an HTML Collection
    // // console.log(cards);
    // console.log(cards[0]);

    // var cardsArr = Array.from(cards); //convert HTML Collection to array

    // cardsArr.shift();
    // console.log(cardsArr);
    // cardsArr.pop();
    // console.log(cardsArr);


    //----------Accessing HTML Elements Using Tag//
    // var sections = document.getElementsByTagName('section');
    // console.log(sections);


    //----------querySelector()
    //returns the first element within that document that matches the specified selector or group of selectors.
    // var headerTitle = document.querySelector('header h1');
                                                // class vs id
                                                // what's your id number #
                                                // what class period .
                                                // # = id
                                                // . = class
    // var headerTitle = document.querySelector('#main-title');

    // console.log(headerTitle);


    //querySelectorAll();

    // var cards = document.querySelectorAll('.card');
    // console.log(cards);

    //-----------Accessing Form

    // var feedbackForm = document.forms['login'];
    // console.log(feedbackForm);

    //-----------Accessing and Modifying elements and properties

    //get value of innerHTML

        // var title = document.getElementById('main-title');
        //
        // console.log(title.innerHTML);

    //console.log(document.getElementById('main-title').innerHTML) //can be used just doesn't look as clean

    //set value of innerHTML

        // title.innerHTML = "Hi <em>Stephen</em>!"

        // console.log()

    //Set value of innerText

    // var title = document.getElementById('main-title');
    // console.log(title.innerText);

    // title.innerText = "Hi <em>MaryAnn</em>!"

    //append value to innerText (works the same with innerHTML)

    // title.innerHTML += "Hi Stephen!"

    //-----------Accessing and modify using attributes

    //check if attribute exists

    // var forms = document.forms['login'];

    // console.log(form);

    // console.log(form.hasAttribute('action'));

    //get attribute value

    // console.log(form.hasAttribute('method'));

    //create a new attribute or change a value of an existing attribute.

        // form.setAttribute('id', 'feedback-form');

        // form.setAttribute('method', 'GET');

    //Delete attribute
    //     form.removeAttribute('action');

        // console.log(form);

    //Accessing and modifying styles

    //single style

        // var jumbotron = document.querySelector('jumbotron');
        // jumbotron.style.display = 'none';

        // jumbotron.style.fontFamily = 'Comic Sans MS';


    //multiple styles
    //     Object.assign(jumbotron.style, {
    //         border : "10px solid black",
    //         fontFamily: "Trajan",
    //         textDecoration: "underline"
    // });

    //Styling Node List

    // var tableRows = document.getElementsByTagName('tr');
    //
    // console.log(tableRows);

    // for (var i = 0; i < tableRows.length; i += 1){
    //
    //     tableRows[i].style.background = 'hotpink';
    // }
    //
    // tableRows.forEach(function () {  //will not work on HTMLcollection
    //     tableRows.style.background = 'blue';
    //
    // })

    // var tableRowArr = Array.from(tableRows); //convert to array

    // tableRowArr.forEach(function (tableRows) {
    //     tableRows.style.background = 'lightblue';

    // })

    //adding and removing elements

        // createElement()
        // removeChild()
        // appendChild()
        // replaceChild()


//***************************************************************************************************

    //------------EVENT LISTENERS
        //addEvenListener()

    //BASIC SYNTAX

    //target.addEventListener(type, listener[,useCapture]);

    //Target  = is the object the event listener is registered on
    //Type  = is the type of event that is being listened for
    //Listener  =  is the function that is called when an even of type happens on the target
    //UseCapture =  is a boolean that decides if the event-capturing is used for event triggering

    //Type of Events
    //     1. keyup (Key is released)
    //     2. keydown (key is pressed down)
    //     3. Click (mouse is clicked)
    //     4. change (input get modified)
    //     5. submit (when form is submitted)
    //     6. hover (when you hover over a key)

    // var testBtn = document.getElementById('testBtn')
    //
    // console.log(testBtn);
    //
    // //add event listener using anonymous function
    //
    // testBtn.addEventListener('click', function () {
    //
    //     if (document.body.style.background === 'red'){
    //         document.body.style.background = 'white'
    //     } else {
    //         document.body.style.background = 'red'
    //     }
    //     console.log('test');
    // });

    //------------add Event Listener from a previously defined function

    // function toggleBackgroundColor() {
    //     if(document.style.background === 'red') {
    //         document.body.style.background = 'hotpink';
    //         testBtn.removeEventListener('click', toggleBackgroundColor)
    //     } else {
    //         document.body.style.background = 'red'
    //     }
    // }
    // testBtn.addEventListener('click', toggleBackgroundColor);


    //------------remove an Event Listener

    // testBtn.removeEventListener('click', toggleBackgroundColor);


    //-----------Register Additional Events

    //cursor hovers over a paragraph, change the color of the text, font-family, and make the font larger

    // var paragraph = document.getElementsByTagName('p')[0];


    //change the font size of whatever 'this' refers to

    // function makeColorChange() {
    //     paragraph.style.color = 'red';
    //     paragraph.style.fontFamily = 'Comic Sans MS';
    //     paragraph.style.fontSize = '30px';
    // }

    //adding a mouse over even to the p that fires off makeColorChange function
    // paragraph.addEventListener('mouseover', makeColorChange);

    //when double clicking the shrink the button, make the font size 1em

    //element reference the shrink btn
    // var shrinkBtn = document.getElementById('shrinkBtn');
    // shrinkBtn.addEventListener('dblclick', function () {
    //     paragraph.style.fontSize = '1em';
    //
    // })

    //--------EVENT OBJECTS

    // Event Object
    // parent to all events- so it will inherit all properties and methods
    // event.target
    // e <-shorter used the event object by declaring the 'e'
    //
    // document.addEventListener('click',function (e) {
    //     var red = e.screenX % 256;
    //     var green = e.screenY & 256;
    //     var blue = 'ff'
    //
    //     console.log(e.screenX);
    //     console.log(e.screenY);
    //
    //     red = red.toString(16);
    //     green = green.toString(16);
    //     blue = blue.toString(16);
    //     document.body.style['background-color'] = '#' + red + green + blue;
    // })


    //Button
    // var button = document.getElementById('btn');
    // button.addEventListener("mousedown", function (e) {
    //     if (e.button == 0){
    //         console.log('left button');
    //     } else if (e.button == 1){
    //         console.log('middle button')
    //     } else if (e.button == 2){
    //         console.log('right button')
    //     }
    // })


    //add a click event to the login btn that fires a function called logUserIn

    // var loginBtn = document.getElementById('login');
    // loginBtn.addEventListener('click', logUserIn)

    // //log in a user
    // function logUserIn() {
    //     //console.log('function was fired!');
    //
    //     //remove the click event on loginBtn once clicked
    //     loginBtn.removeEventListener('click', logUserIn)
    //
    //     alert("Welcome to my site! Please log in!")
    //
    //     var age = prompt('You must be older than 18 to enter. Please enter your age: ');
    //
    //     if (age >= 18){
    //         var answer = confirm('You are being redirected to a really weird site. Do you wish to continue?')
    //
    //     if (answer) {
    //         window.location.replace("https://www.catifpage.com/where-is-the-ball")
    //     }else {
    //         alert('You made the right choice')
    //     }
    // } else if (age === null) {
    //         alert('Well you can\'t enter if you don\'t tell me your age!')
    //     } else if(isNaN(parseInt(age))) {
    //         alert("Stop being a troll");
    //     } else {
    //         alert('Sorry, wait until you grow up')
    //     }
    // }

















})()